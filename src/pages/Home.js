import React, { useContext, useEffect } from "react";
import { ShopContext } from "../context/shopContext";
import { Link } from "react-router-dom";
import { Box, Grid, Text, Image } from "@chakra-ui/react";
import Hero from "../components/Hero";
import ImageWithText from "../components/ImageWithText";
import RichText from "../components/RichText";
const Home = () => {
  const { fetchAllProducts, products } = useContext(ShopContext);

  useEffect(() => {
    fetchAllProducts();
  }, [fetchAllProducts]);

  if (!products) return <div>Loading...</div>;

  return (
    <Box>
      <Hero />
      <RichText
        heading="The relaxation you've been waiting for"
        text="Our Bath bombs guarantee a fun,ralaxing and colorful night."
      />
      <Grid templateColumns="repeat(3,1fr)">
        {products.map((product) => (
          <Link to={`/products/${product.handle}`} key={product.title}>
            <Box
              _hover={{ opacity: "80%" }}
              textAlign="center"
              position="relative"
            >
              <Image src={product.images[0].src} />
              <Text position="absolute" bottom="15%" w="100%" fontWeight="bold">
                {product.title}
              </Text>
              <Text position="absolute" bottom="5%" w="100%">
                ${product.variants?.[0]?.price?.amount}
              </Text>
            </Box>
          </Link>
        ))}
      </Grid>
      <RichText heading="Treat YourSelf!!" />
      <ImageWithText
        image="https://cdn.shopify.com/s/files/1/0869/6630/5080/files/skate_85160b9a-9fd2-418c-8cdf-84a755c5cc46.png?v=1709880867"
        heading="React with shopify headless CMS"
        text="Here is a demo of eact with shopify headless CMS learning"
      />
      <ImageWithText
        reverse
        image="https://cdn.shopify.com/s/files/1/0869/6630/5080/files/skate_85160b9a-9fd2-418c-8cdf-84a755c5cc46.png?v=1709880867"
        heading="React with shopify headless CMS"
        text="Here is a demo of eact with shopify headless CMS learning"
      />
    </Box>
  );
};
export default Home;
