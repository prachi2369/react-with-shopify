import {
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
  VStack,
} from "@chakra-ui/react";
import { Link, useNavigate } from "react-router-dom";
import React, { useContext } from "react";
import { ShopContext } from "../context/shopContext";
const NavMenu = () => {
  const { isMenuOpen, closeMenu } = useContext(ShopContext);
  const navigate = useNavigate();
  const handleMenuClick = (url) => {
    closeMenu();
    navigate(url);
  };
  return (
    <>
      <Drawer
        isOpen={isMenuOpen}
        placement="left"
        size="sm"
        onClose={closeMenu}
      >
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton />
          <DrawerHeader>Menu</DrawerHeader>

          <DrawerBody>
            <VStack p="2rem">
              <Link to="/about" onClick={() => handleMenuClick("/about")}>
                About Us
              </Link>
              <Link
                to="/products/long-sleeve-t-shirt"
                onClick={() => handleMenuClick("/products/long-sleeve-t-shirt")}
              >
                Long Sleeve T-shirts
              </Link>
              <Link
                to="/products/shoes"
                onClick={() => handleMenuClick("/products/shoes")}
              >
                Shoes
              </Link>
            </VStack>
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </>
  );
};
export default NavMenu;
