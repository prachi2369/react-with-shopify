import { Box, Text } from "@chakra-ui/react";
import React from "react";
const Footer = () => {
  return (
    <Box backgroundColor="#FFA8E2">
      <Box>
        <Text
          textAlign="center"
          color="white"
          w="100%"
          borderTop="1px solid white"
          p="1rem"
        >
          @Copyright www.workingwithshopify.com
        </Text>
      </Box>
    </Box>
  );
};
export default Footer;
