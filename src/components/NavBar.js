import { Badge, Box, Flex, Icon, Image } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import React, { useContext } from "react";
import { MdMenu, MdShoppingBasket } from "react-icons/md";
import { ShopContext } from "../context/shopContext";
const NavBar = () => {
  const { openCart, openMenu, checkout } = useContext(ShopContext);
  return (
    <Flex
      backgroundColor="#FFA8E2"
      flexDir="row"
      alignItems="center"
      justifyContent="space-between"
      p="2rem"
    >
      <Icon
        fill="white"
        cursor="pointer"
        as={MdMenu}
        w={30}
        h={30}
        onClick={() => openMenu()}
      ></Icon>
      <Link to="/">
        <Image
          src="https://cdn.shopify.com/s/files/1/0869/6630/5080/files/FaviconSmall.png?v=1709880763"
          w={30}
          h={30}
        />
      </Link>
      <Box>
        <Icon
          fill="white"
          cursor="pointer"
          as={MdShoppingBasket}
          w={30}
          h={30}
          onClick={() => openCart()}
        />
        <Badge backgroundColor="#FF38BD" borderRadius="50%">
          {checkout.lineItems?.length}
        </Badge>
      </Box>
    </Flex>
  );
};
export default NavBar;
