import { Box, Button, Center, Image, Text } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
const Hero = () => {
  const [isMobile, setIsMobile] = useState(window.innerWidth <= 768); // Adjust breakpoint as needed

  useEffect(() => {
    const handleResize = () => setIsMobile(window.innerWidth <= 768);
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return (
    <Box backgroundColor="#FFA8E2" w="100%" position="relative" h="70vh">
      <Image
        src={
          isMobile
            ? "https://cdn.shopify.com/s/files/1/0869/6630/5080/files/coffee.png?v=1709894903"
            : "https://cdn.shopify.com/s/files/1/0869/6630/5080/files/drums-banner.png?v=1709895081"
        }
        h="100%"
        m="auto"
        objectFit="cover"
        objectPosition={["top", "center"]}
      />
      <Text
        className="tracking-in-expand"
        position="absolute"
        bottom={["50%", "30%"]}
        w="100%"
        textAlign="center"
        color="white"
        fontWeight="bold"
        fontSize={["2rem", "4rem"]}
      >
        Introducing New arrival shoes
      </Text>
      <Center>
        <Button
          w="10rem"
          backgroundColor="#FF38BD"
          color="white"
          _hover={{ opacity: "70%" }}
          position="absolute"
          bottom={["40%", "10%"]}
        >
          Shop Now
        </Button>
      </Center>
    </Box>
  );
};
export default Hero;
