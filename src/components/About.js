import { Box, Heading, Text } from "@chakra-ui/react";

const About = () => {
  return (
    <Box>
      <Heading p="2rem" textAlign="center">
        About US
      </Heading>
      <Text p="2rem">
        In 1960, Carl Reiner shot the pilot for a comedy series titled “Head of
        the Family,” starring Carl Reiner. The show was about a writer who lived
        with his family in New Rochelle, New York and commuted to his job in
        Manhattan, writing on a TV comedy show. “Head of the Family” aired only
        once. So Carl had moved on, until Sheldon Leonard came along and said
        they would not fail this time because, “we’ll get a better actor to play
        you.” And thus, “The Dick Van Dyke Show” was born. “Why and When ‘The
        Dick Van Dyke Show’ Was Born” is a recounting of behind-the-scenes
        stories, many of which only Carl could know, about the casting, writing
        and shooting of “The Dick Van Dyke Show.” It includes the real life
        happenings in the lives of Carl and his family and friends, which
        inspired many of the episodes, often proving that truth is exactly as
        strange as fiction – like the episode where Ritchie had to ward off a
        bullying woodpecker. It also includes the real life happenings
        surrounding “The Dick Van Dyke Show,” with all the wonderful actors who
        brought these beloved characters into our homes every week from 1961 to
        1966 – Dick Van Dyke, Mary Tyler Moore, Rose Marie, Morey Amsterdam,
        Richard Deacon, Jerry Paris, Ann Morgan Guilbert, Larry Mathews, all the
        talented guest players and, of course, Carl Reiner as Alan Brady. “Why
        and When ‘The Dick Van Dyke Show’ Was Born” is a must-read for any fan
        of the show, and we will just say, thank you, Carl, for the birth of
        this smart, sophisticated, groundbreaking comedy, a birth which we
        continue to celebrate over five decades later.
      </Text>
    </Box>
  );
};
export default About;
